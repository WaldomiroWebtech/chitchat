Rails.application.routes.draw do
  get 'page/home'
  resources :grupo_relacao_usuarios
  resources :grupos
  resources :usuarios

  mount ActionCable.server =>'/cable'

  root to: 'usuarios#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
