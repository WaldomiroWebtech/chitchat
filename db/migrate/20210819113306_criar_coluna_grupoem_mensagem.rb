class CriarColunaGrupoemMensagem < ActiveRecord::Migration[5.2]
  def change
    add_reference :mensagems, :grupo, foreign_key: true
    add_reference :mensagems, :usuario, foreign_key: true
  end
end
