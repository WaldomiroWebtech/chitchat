class AddDmGrupo < ActiveRecord::Migration[5.2]
  def change
    add_column :grupos, :dm, :boolean, default: false
    add_column :usuarios, :status, :string, default: "Offline"
  end
end
