class Grupo < ApplicationRecord
  has_many :usuarios
  has_many :grupo_relacao_usuarios, dependent: :delete_all
end
