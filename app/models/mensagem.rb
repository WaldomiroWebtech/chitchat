class Mensagem < ApplicationRecord
  after_create_commit{ ChatBroadcastJob.perform_later self, Usuario.find(self.usuario_id), self.grupo_id }
  belongs_to :grupo
  belongs_to :usuario
end
