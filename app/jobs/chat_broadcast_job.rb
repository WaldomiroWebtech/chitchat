class ChatBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message,usuario,sala)
    if message.nil?
      if usuario.status == "Offline"
        js="$('[data-appearing-on = #{usuario.id.to_s}]')[0].classList.remove('text-success');$('[data-appearing-on= #{usuario.id.to_s} ]')[0].classList.add('text-white-50');"
      else
        js="$('[data-appearing-on = #{usuario.id.to_s}]')[0].classList.remove('text-white-50');$('[data-appearing-on = #{usuario.id.to_s}]')[0].classList.add('text-success');"
      end
      ActionCable.server.broadcast "room_channel#{sala}", status: js
    else
      ActionCable.server.broadcast "room_channel#{sala}", message: render_message(message,usuario)
    end

  end


  private
  def render_message(message,usuario)
    ApplicationController.renderer.render(partial: 'mensagens/mensagens', locals: { message: message,usuario: usuario })
  end

end
