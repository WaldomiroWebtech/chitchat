class GruposController < ApplicationController
  before_action :set_grupo, only: %i[ show edit update destroy ]

  include ApplicationHelper
  # GET /grupos or /grupos.json
  def index
    @grupos = Grupo.all
  end

  # GET /grupos/1 or /grupos/1.json
  def show
    @messages = Mensagem.where("grupo_id=#{@grupo.id}")
    @permisao = GrupoRelacaoUsuario.
      where("grupo_id=#{@grupo.id} ").
      where("usuario_id=#{session[:user]['id']}")

  end

  # GET /grupos/new
  def new
    @grupo = Grupo.new
    @relacao = GrupoRelacaoUsuario.new
    @dm = params[:dm]
    @usuarios = Usuario.where.not("id=#{session[:user]['id']}")
  end

  # GET /grupos/1/edit
  def edit
  end

  # POST /grupos or /grupos.json
  def create
    @grupo = Grupo.new( {:nome=> grupo_params['nome'], :dm => grupo_params['dm']} )
  
    @usuario = Usuario.find(session[:user]['id'])
    
    @relacao = GrupoRelacaoUsuario.new({:usuario => @usuario,:grupo => @grupo})

    if grupo_params['dm'] == "true"
      @usuario2 = Usuario.find( grupo_params[:usuario_id]) 
      @relacao2 = GrupoRelacaoUsuario.new({ :usuario => @usuario2 , :grupo => @grupo})
      @relacao2.save
    end  

    respond_to do |format|
      if @grupo.save
        format.html { redirect_to @grupo, notice: "Grupo was successfully created." }
        format.json { render :show, status: :created, location: @grupo }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @grupo.errors, status: :unprocessable_entity }
      end
    end

    @relacao.save
  

    update_grupos(@usuario.id)
  end

  # PATCH/PUT /grupos/1 or /grupos/1.json
  def update
    respond_to do |format|
      if @grupo.update(grupo_params)
        format.html { redirect_to @grupo, notice: "Grupo was successfully updated." }
        format.json { render :show, status: :ok, location: @grupo }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @grupo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grupos/1 or /grupos/1.json
  def destroy
    @grupo.destroy
    respond_to do |format|
      format.html { redirect_to grupos_url, notice: "Grupo was successfully destroyed." }
      format.json { head :no_content }
    end
    relacoes_grupos=GrupoRelacaoUsuario.where("usuario_id = #{session[:user]['id']}")
    grupos_do_usuario=[]
    relacoes_grupos.each do|grupo_relacao|
      grupos_do_usuario << Grupo.where("id = #{grupo_relacao.grupo_id}").first()
    end

    session[:grupos] = grupos_do_usuario
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_grupo
      @grupo = Grupo.find(params[:id])

    end

    # Only allow a list of trusted parameters through.
    def grupo_params
      params.require(:grupo).permit(:nome,:dm,:usuario_id)
    end
end
