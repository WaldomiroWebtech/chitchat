class GrupoRelacaoUsuariosController < ApplicationController
  before_action :set_grupo_relacao_usuario, only: %i[ show edit update destroy ]

  # GET /grupo_relacao_usuarios or /grupo_relacao_usuarios.json
  def index
    @grupo_relacao_usuarios = GrupoRelacaoUsuario.all
  end

  # GET /grupo_relacao_usuarios/1 or /grupo_relacao_usuarios/1.json
  def show
  end

  # GET /grupo_relacao_usuarios/new
  def new
    @grupo_relacao_usuario = GrupoRelacaoUsuario.new
    @grupos = session[:grupos]
    @usuarios = Usuario.where.not("id=#{session[:user]['id']}")
  end

  # GET /grupo_relacao_usuarios/1/edit
  def edit
  end

  # POST /grupo_relacao_usuarios or /grupo_relacao_usuarios.json
  def create
    @grupo_relacao_usuario = GrupoRelacaoUsuario.new(grupo_relacao_usuario_params)

    respond_to do |format|
      if @grupo_relacao_usuario.save
        format.html { redirect_to "/usuarios/#{session[:user]['id']}", notice: "Grupo relacao usuario was successfully created." }
        format.json { render :show, status: :created, location: @grupo_relacao_usuario }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @grupo_relacao_usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /grupo_relacao_usuarios/1 or /grupo_relacao_usuarios/1.json
  def update
    respond_to do |format|
      if @grupo_relacao_usuario.update(grupo_relacao_usuario_params)
        format.html { redirect_to @grupo_relacao_usuario, notice: "Grupo relacao usuario was successfully updated." }
        format.json { render :show, status: :ok, location: @grupo_relacao_usuario }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @grupo_relacao_usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grupo_relacao_usuarios/1 or /grupo_relacao_usuarios/1.json
  def destroy
    @grupo_relacao_usuario.destroy
    respond_to do |format|
      format.html { redirect_to grupo_relacao_usuarios_url, notice: "Grupo relacao usuario was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_grupo_relacao_usuario
      @grupo_relacao_usuario = GrupoRelacaoUsuario.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def grupo_relacao_usuario_params
      params.require(:grupo_relacao_usuario).permit(:usuario_id, :grupo_id)
    end
end
