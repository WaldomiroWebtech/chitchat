json.extract! grupo_relacao_usuario, :id, :usuario_id, :grupo_id, :created_at, :updated_at
json.url grupo_relacao_usuario_url(grupo_relacao_usuario, format: :json)
