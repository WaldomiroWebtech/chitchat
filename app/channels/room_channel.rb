# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class RoomChannel < ApplicationCable::Channel
  def subscribed
    stream_from "room_channel#{params[:sala]}"
    if current_user.status != "Online"
      current_user.update(status: "Online")
      ChatBroadcastJob.perform_now(nil,current_user,params[:sala])
    end
  end

  def unsubscribed
    if current_user.status != "Offline"
      current_user.update(status: "Offline")
      ChatBroadcastJob.perform_now(nil,current_user,params[:sala])
    end
  end

  def speak(data)
    grupo = Grupo.find(data['group'].to_i)
    usuario = Usuario.find(data['author'].to_i)

    raise 'No message!' if data['message'].blank?

    Mensagem.create! conteudo: data['message'], grupo: grupo, usuario: usuario
  end


end