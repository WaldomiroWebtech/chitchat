module ApplicationHelper
  def update_grupos(user_id)
    relacoes_grupos=GrupoRelacaoUsuario.where("usuario_id = #{user_id}")
    grupos_do_usuario=[]
    dm_do_usuario=[]
    contatos=[]
    relacoes_grupos.each do|grupo_relacao|
      grupo = Grupo.where("id = #{grupo_relacao.grupo_id}").first()
      usuario = GrupoRelacaoUsuario.where("grupo_id = #{grupo.id}").where.not("usuario_id= #{user_id}").first().usuario_id
      if(grupo.dm == false)
        grupos_do_usuario << grupo
      else
        dm_do_usuario << grupo
        contatos <<  Usuario.find(usuario)
      end
    end

    session[:grupos] = grupos_do_usuario
    session[:dm] = { grupo: dm_do_usuario, usuario: contatos}
  end
end
