require 'test_helper'

class GrupoRelacaoUsuariosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @grupo_relacao_usuario = grupo_relacao_usuarios(:one)
  end

  test "should get index" do
    get grupo_relacao_usuarios_url
    assert_response :success
  end

  test "should get new" do
    get new_grupo_relacao_usuario_url
    assert_response :success
  end

  test "should create grupo_relacao_usuario" do
    assert_difference('GrupoRelacaoUsuario.count') do
      post grupo_relacao_usuarios_url, params: { grupo_relacao_usuario: { grupo_id: @grupo_relacao_usuario.grupo_id, usuario_id: @grupo_relacao_usuario.usuario_id } }
    end

    assert_redirected_to grupo_relacao_usuario_url(GrupoRelacaoUsuario.last)
  end

  test "should show grupo_relacao_usuario" do
    get grupo_relacao_usuario_url(@grupo_relacao_usuario)
    assert_response :success
  end

  test "should get edit" do
    get edit_grupo_relacao_usuario_url(@grupo_relacao_usuario)
    assert_response :success
  end

  test "should update grupo_relacao_usuario" do
    patch grupo_relacao_usuario_url(@grupo_relacao_usuario), params: { grupo_relacao_usuario: { grupo_id: @grupo_relacao_usuario.grupo_id, usuario_id: @grupo_relacao_usuario.usuario_id } }
    assert_redirected_to grupo_relacao_usuario_url(@grupo_relacao_usuario)
  end

  test "should destroy grupo_relacao_usuario" do
    assert_difference('GrupoRelacaoUsuario.count', -1) do
      delete grupo_relacao_usuario_url(@grupo_relacao_usuario)
    end

    assert_redirected_to grupo_relacao_usuarios_url
  end
end
