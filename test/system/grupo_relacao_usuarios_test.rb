require "application_system_test_case"

class GrupoRelacaoUsuariosTest < ApplicationSystemTestCase
  setup do
    @grupo_relacao_usuario = grupo_relacao_usuarios(:one)
  end

  test "visiting the index" do
    visit grupo_relacao_usuarios_url
    assert_selector "h1", text: "Grupo Relacao Usuarios"
  end

  test "creating a Grupo relacao usuario" do
    visit grupo_relacao_usuarios_url
    click_on "New Grupo Relacao Usuario"

    fill_in "Grupo", with: @grupo_relacao_usuario.grupo_id
    fill_in "Usuario", with: @grupo_relacao_usuario.usuario_id
    click_on "Create Grupo relacao usuario"

    assert_text "Grupo relacao usuario was successfully created"
    click_on "Back"
  end

  test "updating a Grupo relacao usuario" do
    visit grupo_relacao_usuarios_url
    click_on "Edit", match: :first

    fill_in "Grupo", with: @grupo_relacao_usuario.grupo_id
    fill_in "Usuario", with: @grupo_relacao_usuario.usuario_id
    click_on "Update Grupo relacao usuario"

    assert_text "Grupo relacao usuario was successfully updated"
    click_on "Back"
  end

  test "destroying a Grupo relacao usuario" do
    visit grupo_relacao_usuarios_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Grupo relacao usuario was successfully destroyed"
  end
end
